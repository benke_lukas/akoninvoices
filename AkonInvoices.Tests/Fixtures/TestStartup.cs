using AkonInvoices.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AkonInvoices.Tests.Fixtures
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        public override void setUpDatabase(IServiceCollection services) {
            services.AddDbContext<AppDbContext>(options => options.UseInMemoryDatabase("test_db"));
        }
    }
}