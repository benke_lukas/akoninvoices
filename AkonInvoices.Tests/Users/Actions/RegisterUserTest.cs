using AkonInvoices.Core.Hasher;
using AkonInvoices.Database;
using AkonInvoices.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AkonInvoices.Tests.Users.Actions
{
    public class RegisterUserTest
    {
        private readonly AkonInvoices.Users.Actions.RegisterUser _registerUser;

        public RegisterUserTest() {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "test_db")
                .Options;

            _registerUser = new AkonInvoices.Users.Actions.RegisterUser(new AppDbContext(options), new PBKDF2Hasher());
        }
        
        [Fact]
        public void ItCanRegisterUser()
        {
            //Given
            
            //When
            User user = _registerUser.Register("User", "1", "user.1@domain.tld", "test");
            
            //Then
            Assert.Equal("User", user.FirstName);
            Assert.Equal("1", user.LastName);
            Assert.Equal("user.1@domain.tld", user.Email);
            Assert.NotEqual("test", user.Password);
        }
    }
}