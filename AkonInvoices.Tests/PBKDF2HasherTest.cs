using AkonInvoices.Core.Hasher;
using AkonInvoices.Core.Hasher.Contracts;
using Xunit;

namespace AkonInvoices.Tests
{
    public class PBKDF2HasherTest
    {
        private readonly IHasher _hasher;

        public PBKDF2HasherTest() {
            _hasher = new PBKDF2Hasher();
        }

        [Fact]
        public void ItCanReturnCorrectHash()
        {
            //Given
            string test = "Test";
            //When
            string hashed = _hasher.Hash(test);
            //Then
            Assert.True(_hasher.Verify(hashed, test));
        }
    }
}