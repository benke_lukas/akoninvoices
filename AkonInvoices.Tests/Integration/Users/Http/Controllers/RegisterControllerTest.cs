using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AkonInvoices.Tests.Fixtures;
using Newtonsoft.Json;
using Xunit;

namespace AkonInvoices.Tests.Integration.Users.Http.Controllers
{
    public class RegisterControllerTest:IClassFixture<TestServerFixture>
    {
        private readonly TestServerFixture _fixture;

        public RegisterControllerTest(TestServerFixture fixture) {
            _fixture = fixture;
        }

        [Fact]
        public async Task ItRegistersUser() {
            var data = new {
                FirstName = "User",
                LastName = "1",
                Email = "user.1@domain.tld",
                Password = "user.1"
            };
            var response = await _fixture.Client.PostAsync("/api/users/register", 
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")
            );

            response.EnsureSuccessStatusCode();
        }
    }
}