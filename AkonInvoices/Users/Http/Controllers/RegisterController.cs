using AkonInvoices.Users.Actions.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AkonInvoices.Users.Http.Controllers
{
    [Route("/api/users/register")]
    [ApiController]
    public class RegisterController : Controller
    {
        private readonly IRegisterUser _registerUser;

        public RegisterController(IRegisterUser registerUser) {
            _registerUser = registerUser;
        }

        [HttpPost("")]
        public IActionResult store([FromBody] Entities.User user) {
            Entities.User registeredUser = _registerUser.Register(user.FirstName, user.LastName, user.Email, user.Password);

            return Ok();
        }
    }
}