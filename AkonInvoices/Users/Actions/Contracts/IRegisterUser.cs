namespace AkonInvoices.Users.Actions.Contracts
{
    public interface IRegisterUser
    {
         Users.Entities.User Register(string FirstName, string LastName, string Email, string Password);
    }
}