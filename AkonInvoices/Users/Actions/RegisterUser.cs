using AkonInvoices.Core.Hasher.Contracts;
using AkonInvoices.Database;
using AkonInvoices.Users.Actions.Contracts;
using AkonInvoices.Users.Entities;

namespace AkonInvoices.Users.Actions {
    public class RegisterUser : IRegisterUser {
        private AppDbContext _context;

        private readonly IHasher _hasher;

        public RegisterUser (AppDbContext context, IHasher hasher) {
            _hasher = hasher;
            _context = context;
            _hasher = hasher;
        }
        public User Register (string FirstName, string LastName, string Email, string Password) {
            User user = new User {
                    FirstName = FirstName,
                    LastName = LastName,
                    Email = Email,
                    Password = _hasher.Hash(Password)
                };
            
            _context.Users.Add (
                user
            );
            _context.SaveChanges();

            return user;
        }
    }
}