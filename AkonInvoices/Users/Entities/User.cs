using System.ComponentModel.DataAnnotations;

namespace AkonInvoices.Users.Entities
{
    public class User
    {
        public int UserId {get; set;}
        [Required]
        public string FirstName {get; set;}
        [Required]
        public string LastName {get; set;}
        [Required]
        [EmailAddress]
        public string Email {get; set;}
        [MinLength(6)]
        [MaxLength(255)]
        public string Password {get;set;}

        public UserAppCredentials AppCredentials {get; set;}
    }
}