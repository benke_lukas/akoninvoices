namespace AkonInvoices.Users.Entities
{
    public class UserAppCredentials
    {
        public int UserAppCredentialsId {get; set;}
        public string Name {get; set;}
        public string Credentials {get; set;}
        
        public int UserId {get; set;}
        public User User {get; set;}
    }
}