using Microsoft.EntityFrameworkCore;
using AkonInvoices.Users.Entities;

namespace AkonInvoices.Database
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {}

        public DbSet<User> Users {get; set;}
    }
}