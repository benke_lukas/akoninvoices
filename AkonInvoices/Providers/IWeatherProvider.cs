using System.Collections.Generic;
using AkonInvoices.Models;

namespace AkonInvoices.Providers
{
    public interface IWeatherProvider
    {
        List<WeatherForecast> GetForecasts();
    }
}
