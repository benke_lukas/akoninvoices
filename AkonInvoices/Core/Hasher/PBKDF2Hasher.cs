using System;
using System.Security.Cryptography;
using AkonInvoices.Core.Hasher.Contracts;

namespace AkonInvoices.Core.Hasher
{
    public class PBKDF2Hasher : IHasher
    {
        private const int hashIterations = 1000;

        public string Hash(string data)
        {
            //create salt value with cryptographic PRNG
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            //create the Rfc2898DerivedBytes and get the hash value
            var pbkdf2 = new Rfc2898DeriveBytes(data, salt, hashIterations);
            byte[] hash = pbkdf2.GetBytes(20);
            //combine salt and password bytes
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            //turn salt+hash into stored hashed password
            string hashed = Convert.ToBase64String(hashBytes);

            return hashed;
        }

        public bool Verify(string hashed, string notHashed) {
            //Extract bytes
            byte[] hashBytes = Convert.FromBase64String(hashed);
            //Get the salt
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            //Compute hash of nonhashed
            var pbkdf2 = new Rfc2898DeriveBytes(notHashed, salt, hashIterations);
            byte[] hash = pbkdf2.GetBytes(20);
            //Compare
            for ( int i = 0; i < 20; i++) {
                if(hashBytes[i+16] != hash[i])
                    return false;
            }
            return true;
        }
    }
}