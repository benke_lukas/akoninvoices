namespace AkonInvoices.Core.Hasher.Contracts
{
    public interface IHasher
    {
         string Hash(string data);
         bool Verify(string hashed, string notHashed);
    }
}